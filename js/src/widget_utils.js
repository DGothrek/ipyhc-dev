const clean_data = (series) => {
    const res = []
    series.forEach(serie => {
        res.push(clean_serie(serie))
    });
    return res;
}

const clean_serie = (serie) => {
    const res = []
    serie.data.forEach(point => {
        res.push({ x: point.x, y: point.y })
    });
    return res;
}

export { clean_data };